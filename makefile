# note: this is not the makefile to create shared libraries of vspline
# code for the bspline modulte! That makefile is in the 'bspline' folder

# this makefile produces six different versions of coordinates.so:
# multithreaded with and without SIMD, and singlethreaded with or without
# SIMD. The singlethreaded versions are needed if vspline's transform
# family of functions are used to 'roll out' python code, which will
# fail when these functions multithread the operation.
# It also produces use_vc.so, which is needed to 'pull in' libVc.a,
# and the analogon using hwy.
# This makefile assumes you have Vc and/or hwy installed. If that is not
# the case, remove the targets linking Vc and/or hwy.

# using '-march=native' will produce code for the current CPU. This
# seems to be a portable way of communicating to the SIMD libraries
# which ISA to emit, but it must match what cling (the compiler behind
# cppyy) is invoked with, so you must set an environment variable to
# that effect as well, use 'export EXTRA_CLING_ARGS="-march=native"'

ccflags=-march=native -std=c++11 -O2 -fPIC -shared

all: coordinates_vc.so coordinates_hwy.so coordinates_novc.so coordinates_vc_st.so coordinates_hwy_st.so coordinates_novc_st.so

coordinates.cc: coordinates.h
	touch coordinates.cc

# no Vc, multithreaded

coordinates_novc.so: coordinates.cc
	clang++ -pthread $(ccflags) -o coordinates_novc.so coordinates.cc

# no Vc, singlethreaded

coordinates_novc_st.so: coordinates.cc
	clang++ -DVSPLINE_SINGLETHREAD $(ccflags) -o coordinates_novc_st.so coordinates.cc

# using Vc, multithreaded

coordinates_vc.so: coordinates.cc
	clang++ -pthread -DUSE_VC $(ccflags) -o coordinates_vc.so coordinates.cc -lVc

# using Vc, singlethreaded

coordinates_vc_st.so: coordinates.cc
	clang++ -DUSE_VC -DVSPLINE_SINGLETHREAD $(ccflags) -o coordinates_vc_st.so coordinates.cc -lVc

# using hwy, multithreaded

coordinates_hwy.so: coordinates.cc
	clang++ -pthread -DUSE_HWY $(ccflags) -o coordinates_hwy.so coordinates.cc -lhwy

# using hwy, singlethreaded

coordinates_hwy_st.so: coordinates.cc
	clang++ -DUSE_HWY -DVSPLINE_SINGLETHREAD $(ccflags) -o coordinates_hwy_st.so coordinates.cc -lhwy
