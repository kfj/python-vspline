#   This is an example program to be used with
#
#   bspline - python module wrapping vspline: generic C++ code
#             for creation and evaluation of uniform b-splines
#   
#           Copyright 2015 - 2021 by Kay F. Jahnke
#   
#   Permission is hereby granted, free of charge, to any person
#   obtaining a copy of this software and associated documentation
#   files (the "Software"), to deal in the Software without
#   restriction, including without limitation the rights to use,
#   copy, modify, merge, publish, distribute, sublicense, and/or
#   sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following
#   conditions:
#   
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the
#   Software.
#   
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#   OTHER DEALINGS IN THE SOFTWARE.

# small program to demonstrate a periodic spline over 3D points
# On a spherical surface. This spline is a good approximation of
# a sphere. The points are arranged so that they correspond with
# a grid of polar coordinates (r==1). This is just one way of
# setting up a suitable spline: We need equally spaced points on
# equally spaced great circles, and I assume at least three great
# circles are needed - so that's a minimum of nine knot points.
# It's easy to play with different grids: just change the first
# two parameters to 'sphspline'.

# Note that this is a simplified case of a NURBS: we have a 2D
# manifold mapped into 3D space. But in contrast to a fully-featured
# NURBS, we have only single control points, all equally spaced,
# and equally weighted - what I call a 'uniform b-spline'. Note
# also that - as far as I understand it - NURBS are not built
# with periodic splines, because they are normally used for small
# rectangular patches. Using a periodic spline for a sphere is, of
# course, most appropriate.

# We establish a few facts:

# - that evaluation of the spline produces points on a sphere
# - that the coorinates at which we evaluate correspond directly
#   to spherical coordinates on the sphere
# - that a spline over geometrically transformed knot points
#   evaluates to equally transformed values

# Using double precision arithmetic, we can observe how the
# approximation gets better with rising spline degree up to the
# thirties, where it's quality stagnates and then deteriorates,
# due to increasing errors.

# We see that the quality of the approximation can also be
# increased by using more knot points.

# As we're not pressed for speed here, none of vspline's 'higher'
# functions are used, we evaluate the spline using it's 'standard'
# evaluator, and do so in a loop.

import math
import bspline
import numpy as np

try:

  from scipy.spatial.transform import Rotation as R
  # rotation to apply for testing

  test_rotation = R.from_quat ( [ .3 , -.7 , 3.41 , 0.01 ] )

except:

  test_rotation = None
  pass

def to_polar ( x , y , z ) :
  phi = math.atan2 ( y , x )
  theta = math.acos ( z )
  return theta , phi

def to_cartesian ( theta , phi ) :
  x = math.sin ( theta ) * math.cos ( phi )
  y = math.sin ( theta ) * math.sin ( phi )
  z = math.cos ( theta )
  return x , y , z

# sphspline creates a b-spline over equally spaced points on a sphere.
# The sampling is done by increasing the polar angles theta and phi
# by regular increments, converting the resulting polar coordinate
# to cartesian and storing that as the knot point value. Evaluating
# the spline produces the cartesian coordinate for a given spline
# coordinate.
# TODO might apply domain

def sphspline ( nx , ny , degree , tf = None ) :

  bsp = bspline.bspline ( base_type = "double" ,
                          channels  = 3 ,
                          dimension = 2 ,
                          shape     = ( nx , ny ) ,
                          degree    = degree ,
                          boundary  = ( bspline.PERIODIC , bspline.PERIODIC ) )

  hstep = 2 * math.pi / nx
  vstep = 2 * math.pi / ny

  for x in range ( nx ) :
    for y in range ( ny ) :

      a , b , c = to_cartesian ( x * hstep , y * vstep )
      if ( tf ) :
        bsp [ ( x , y ) ] = tf ( a , b , c )
      else :
        bsp [ ( x , y ) ] = ( a , b , c )

      #print ( "x %f y %f z %f" %
             #( bsp[(x,y)][0] , bsp[(x,y)][1] , bsp[(x,y)][2] ) )

  bsp.prefilter()
  return bsp

# here's the test routine:

def test ( bsp , ptf ) :

  # create 1000 random 2D coordinates

  arr = np.random.rand ( 1000 , 2 ) ;
  arr *= 100 ;
  arr -= 50

  # we want average and maximum delta

  sum = 0
  maximum = 0

  # we're using the spline's default evaluator, which is a 'safe'
  # evaluator folding out-of range values into the defined range. Since
  # the spline is periodic, the evaluator created by the factory function
  # uses periodic folding, so we can feed arbitrary 2D coordinates.

  for i in range(1000) :
    a,b,c = bsp ( arr[i] )
    d = math.sqrt ( a * a + b * b + c * c )
    delta = abs ( 1 - d )
    if delta > maximum :
      maximum = delta
    sum += delta

  # echo result. We can see that the evaluation has indeed produced
  # points (roughly) on a sphere.
  
  sum /= 1000
  print ( "test 1:   degree: %d average delta: %.18f max delta: %.18f " %
           ( degree , sum , maximum ) )

  
  # The second test demonstrates that we have a direct proportionality
  # between these (x,y) coordinates and polar coordinates: the relation
  # doesn't only hold at the knot points.
  
  sum = 0
  maximum = 0

  for i in range(1000) :

    # adapt range from [0,nx] / [0,ny] to [0, 2 pi]

    theta = 2.0 * math.pi * arr[i][0] / bsp.shape[0]
    phi = 2.0 * math.pi * arr[i][1] / bsp.shape[1]

    # obtain cartesian coordinate via conventional route:

    x , y , z = to_cartesian ( theta , phi )

    # obtain cartesian coordinate using the spline:
 
    a , b , c = bsp ( arr [ i ] )

    # compare both results

    dx = x - a
    dy = y - b
    dz = z - c
    delta = math.sqrt ( dx * dx + dy * dy + dz * dz )
    if delta > maximum :
      maximum = delta
    sum += delta

  # echo result

  sum /= 1000
  print ( "test 2:   degree: %d average delta: %.18f max delta: %.18f " %
           ( degree , sum , maximum ) )

  # third test: we assume that, if we submit the knot points to a
  # geometrical transformation 'tf', the resulting spline will produce
  # tf(r) if the original spline produced r, when evaluated at (x,y).
  # For affine (check) transformation, this turns out to be true, and it
  # (TODO) may be true for other transformations - I assume it will work
  # as long as these transformations don't have discontinuities.
  # This is an interesting aspect: we can 'accumulate' any number of
  # transformations in the knot points and evaluate the spline over
  # the transformed knot points instead of having to perform the
  # transformations on each point in question. While the evaluation of
  # the spline (for higher degrees) is quite expensive, it may still
  # turn out advantageous if the transformations are complex, and the
  # spline evaluation in vspline is highly optimized and does not need
  # transcendental functions.

    # create the spline with transformed knot points

  tftest = 0

  for tf in ptf:

    tftest += 1
    bsptf = sphspline ( bsp.shape[0] , bsp.shape[1] , degree , tf )

    # now produce the results

    sum = 0
    maximum = 0

    for i in range(1000) :

      # transform result of b-spline evaluation

      r1 = tf ( * bsp ( arr[i] ) )

      # evaluate transformed b-spline

      r2 = bsptf ( arr[i] )

      # compare both results
      
      dx = r2[0] - r1[0]
      dy = r2[1] - r1[1]
      dz = r2[2] - r1[2]
      delta = math.sqrt ( dx * dx + dy * dy + dz * dz )
      if delta > maximum :
        maximum = delta
      sum += delta

    # echo result

    sum /= 1000
    print ( "test 3.%d: degree: %d average delta: %.18f max delta: %.18f " %
            ( tftest , degree , sum , maximum ) )

# run this test for all spline degrees. Why is 0 looking so good?
# nearest neighbour yields one of the knot points, which were chosen
# to lie on the sphere, hence the precise result.

for degree in range ( 0 , 46 ) :

  # we can use any number for nx, ny:

  sph = sphspline ( 12 , 9 , degree )

  def translate ( x , y , z ) :
    return x + 10.3 , y - 7.1 , z + .55

  def scale ( x , y , z ) :
    return x * 10.3 , y * -7.1 , z * .55

# rotation relies on scipy - if scipy is missing, pass

  def rotate ( x , y , z ) :
    if test_rotation is None :
      return x , y , z
    else :
      return test_rotation.apply ( [ x , y , z ] )

  def shear ( x , y , z ) :
    return ( x + .77 * y , y - 1.9 * z , z + .333 * y - .55 * x )

  def combined ( x , y , z ) :
    return rotate ( * shear ( * scale ( * translate ( x , y , z ) ) ) )

  test ( sph , ( translate , scale , rotate , shear , combined ) )
