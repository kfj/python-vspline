// C++ code to build the shared library 'vspline_binary.so' to be used by
// 'vspline.py'. The shared library provides precompiled binary code for
// most of vspline's functionality, reducing 'warmup' times.
// Use of vspline_binary.so is optional, vspline.py will run without
// it, but some code may take a good while to run on first call, as it
// has to be compiled on-demand by cppyy; having the binary code handy in
// vspline_binary.so makes this on-demand compilation unnecessary for
// code covered by vspline_binary.so. Note as well that the binary code
// cppyy produces on demand perishes with the cppyy instance, so warmup
// time will be needed for the next time your program is run with a
// fresh import of vspline.py.

// here, I've built the .so by executing:

// clang++ -DUSE_VC -Wno-abi -std=c++11 -O3 -march=native -fPIC -shared -o vspline_binary.so vspline_binary.cc -lVc

// since I have Vc installed. This requires a Vc checkout of the 1.3 branch.
// Vc can be omitted, producing slightly less performant code on my system:

// clang++ -Wno-abi -std=c++11 -O3 -march=native -fPIC -shared -o vspline_binary.so vspline_binary.cc

// With the default scope of vspline_binary.so this will take quite some
// time: expect something in the order of magnitude of minutes. The mere size
// of the headers is deceptively small.

// we use 'vspline_binary.h', but by defining EMIT_DEFINITIONS, we get the
// definitions in stead of the declarations. The only difference between the
// declarations and the definitions is the use of 'extern'. When modifying the
// scope of vspline_binary.so, start out in set_type.h and work your way 'down'.
// This is simple copy and paste, or uncommenting, so it's easy to do even if
// you don't know C++. The default set of dimensions, channels and types is rather
// generous, you may want to cut it down if space and compile times are an issue.

// TODO: instead of using a monolithic header and .so, the individual
// 'constellations' might be separated, which would produce a more modular
// design and reduce compile time, since the constellations could each be
// compiled by an individual thread.

#define EMIT_DEFINITIONS

#include "vspline_binary.h"

#undef EMIT_DEFINITIONS
