// this trivial one-liner 'pulls in' all of the Vc code living in libVc.a.
// It's used to create a shared library, 'get_vc.so', which vspline.py
// looks for to determine whether Vc is available or not.

// compile like this:

// clang++ -march=native -std=c++11 -O3 -fPIC -shared -o get_vc.so get_vc.cc -lVc

#include <Vc/Vc>
