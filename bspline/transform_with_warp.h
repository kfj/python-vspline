// this header is used to provide specialized declarations/definitions
// of a transform taking a coordinate array and storing the result in
// another array of the same shape. Here, we have an additional degree
// of freedom, namely the dimensionality of the coordinate/target array:
// we might have a 1, 2 or 3D array of nD coordinates, producing another
// array of corresponding results. See 'declare.h' where TRG_DIMENSION is
// set to - currently - 1, 2 and 3 before #includeing this header.

PREFIX template
void vspline::transform < ev_type , TRG_DIMENSION > 
     ( const ev_type & functor ,
       const vigra::MultiArrayView
           < TRG_DIMENSION ,
             typename ev_type::in_type > & ,
       vigra::MultiArrayView
           < TRG_DIMENSION ,
             typename ev_type::out_type > &  ,
       int njobs = vspline::default_njobs ,
       vspline::atomic < bool > * p_cancel = 0 ) ;
