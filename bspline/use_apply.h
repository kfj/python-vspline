// this header adds code to vspline_binary.so to 'apply'
// a functor to data arrays by calling vspline::apply.
// This is optional and the resulting wrap of vspline::apply
// is not currently used by vspline.py, which directly uses
// vspline::transform instead.

// If definitions instead of declarations are wanted, we drop the 'extern'
// prefix by defining PREFIX empty - for definitions we use 'extern'.
// Since we're not actually providing any specialized code but merely
// triggering the compilation of specific code into the shared library,
// declarations and definitions are identical otherwise.

#ifdef EMIT_DEFINITIONS
#define PREFIX
#else
#define PREFIX extern
#endif

#define data_type vigra::TinyVector < TYPE , DIMENSION >
#define ev_type vspline::grok_type < data_type , data_type >

PREFIX template
void vspline::apply < ev_type , DIMENSION >
     ( const ev_type & functor ,
       vigra::MultiArrayView
           < DIMENSION ,
             typename data_type > & ,
       int njobs = vspline::default_njobs ,
       vspline::atomic < bool > * p_cancel = 0
     ) ;

#undef ev_type
#undef data_type
#undef PREFIX
