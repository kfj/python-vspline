#include "vspline/vspline.h"
#include <iostream>

namespace vspline
{
/// this class template inherits from vigra::MultiArrayView and takes a
/// slightly different template argument signature, which fits well with
/// vspline: vspline can handle fundamental types and aggregates of several
/// identical fundamentals, which are internally treated as vigra::TinyVectors.
/// So this class takes the dimensionality of the view, the fundamental type
/// and the number of channels as
/// template arguments, and accepts a pointer to the fundamental data type in
/// it's constructor, which can be delivered by the buffer protocol when passing
/// in NumPy arrays from Python. While passing NumpyArrays like this does work,
/// it's error-prone, because Numpy arrays are the mightier type and they can
/// represent arrays which can't be represented by a vigra::MultiArrayView.
/// Be that as it may, Numpy compatibility is a separate issue, and for the
/// time being I don't focus on it; I reckon that eventually I'll provide a
/// python data type encapsulating a multi_array_view and provide the
/// appropriate buffer protocol data, which is easy. If users need to pass
/// Numpy arrays, additional code would be needed to translate suitable
/// NumpyArrays or reject unsuitable ones.
///
/// Note that shape and strides are measured in units of value_type and not
/// in units of bytes (which is NumPy convention) or fundamental_type.
///
/// Note that vigra MultiArrayViews use nD indices with the 'fastest moving index'
/// first, which is the opposite order to C and default Numpy arrays.

template < typename T , int N >
void prtv ( const vigra::TinyVector < T , N > & tv )
{
  std::cout << tv << std::endl ;
}

// tag class to check if the instantiated template is a multi_array_view

class is_multi_array_view {} ;

template < int _dimension , typename _fundamental_type , int _channels >
struct multi_array_view
: public vigra::MultiArrayView
         < _dimension ,
           canonical_type < _fundamental_type , _channels >
         > ,
  public is_multi_array_view
{
  enum { dimension = _dimension } ;
  enum { channels = _channels } ;
  typedef _fundamental_type fundamental_type ;
  enum { itemsize = sizeof ( fundamental_type ) } ;
  
  typedef canonical_type < fundamental_type , channels > value_type ;
  typedef vigra::MultiArrayView < dimension , value_type > base_type ;
  typedef vigra::TinyVector < long , dimension > shape_type ;

  // the base address as a long integer is required for python's
  // __array_interface__

  long baseaddr ;

  // as is the typestring.

  const char * get_typestr()
  {
    if ( std::is_same < fundamental_type , short > :: value )
      return "<i2" ;
    else if ( std::is_same < fundamental_type , int > :: value )
      return "<i4" ;
    else if ( std::is_same < fundamental_type , long > :: value )
      return "<i8" ;
    else if ( std::is_same < fundamental_type , float > :: value )
      return "<f4" ;
    else if ( std::is_same < fundamental_type , double > :: value )
      return "<f8" ;
    else
      return "???" ;
  }

  multi_array_view ( const shape_type & shape ,
                     const shape_type & stride ,
                     fundamental_type * data )
  : vigra::MultiArrayView < dimension , value_type >
          ( shape , stride , (value_type*) data ) ,
    baseaddr ( long ( data ) )
  { } ;

  multi_array_view ( const shape_type & shape ,
                     fundamental_type * data )
  : vigra::MultiArrayView < dimension , value_type >
          ( shape , (value_type*) data ) ,
    baseaddr ( long ( data ) )
  { } ;

  multi_array_view ( vigra::MultiArrayView < dimension , value_type > & other )
  : vigra::MultiArrayView < dimension , value_type > ( other ) ,
    baseaddr ( long ( other.data() ) )
  { } ;
  
//   const value_type & get ( const shape_type & crd )
//   {
//     return (*this)[crd] ;
//   }
//   
//   void set ( const shape_type & crd , const value_type & v )
//   {
//     (*this)[crd] = v ;
//   }
} ;

} ;
