// master header for vspline_binary, pushing the specializations into a
// separate header 'declare.h', which produces different specializations
// every time it is included, since CHANNELS, DIMENSION and TYPE are set
// in successive sub-includes, finally including 'declare.h', which has
// the actual declaration/definition code.
//
// I admit that I made a big fuss about this method, which is used in hugin
// and dubbed 'lazy metaprograming' there, because at the time I was wrapping
// hugin's headers with swig, and swig does not go beyond the first header.
// With cppyy, the headers are pulled in all the way 'down', so it's okay.
//
// For now, I provide precompiled code for single and double precision
// values in up to three dimensions, with up to three channels.
// This is merely to have a few options to play with and make sure the
// mechanism works as intended.

// First, #include 'all of' vspline:

#include "vspline/vspline.h"

// Now generate declarations/definitions for vspline_binary.so

#include "set_type.h"

typedef vigra::TinyVector < float , 2 > crd_t ;

#ifdef EMIT_DEFINITIONS

// vc_in_use() is an introspective function which returns true if USE_VC
// is defined when this header is included, and false otherwise. It's
// called by vspline.py to figure out whether USE_VC has to be defined
// inside the python module as well, to create compatible declarations
// to the binary code in vspline_binary.so.

#ifdef USE_VC

bool vc_in_use() { return true ; }

#else

bool vc_in_use() { return false ; }

#endif

#ifdef USE_HWY

bool hwy_in_use() { return true ; }

#else

bool hwy_in_use() { return false ; }

#endif

#else // #ifdef EMIT_DEFINITIONS

extern bool vc_in_use() ;
extern bool hwy_in_use() ;

#endif // #ifdef EMIT_DEFINITIONS


