#   bspline - a python module wrapping 'vspline: generic C++ code
#             for creation and evaluation of uniform b-splines'
#   
#           Copyright 2015 - 2021 by Kay F. Jahnke
#   
#   Permission is hereby granted, free of charge, to any person
#   obtaining a copy of this software and associated documentation
#   files (the "Software"), to deal in the Software without
#   restriction, including without limitation the rights to use,
#   copy, modify, merge, publish, distribute, sublicense, and/or
#   sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following
#   conditions:
#   
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the
#   Software.
#   
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#   OTHER DEALINGS IN THE SOFTWARE.

# this is a python module wrapping vspline, a generic C++ library
# for uniform b-spline processing.
# The module can be configured, please have a look at configure.py.

import time

def log ( message ) :

  previous = time.perf_counter()
  print ( "%2.6f %s" % ( previous , message ) )

#log ( "vspline: importing cppyy" )

import cppyy
import numpy as np

# the config module pulls in shared libraries and sets some defines
# This may fail with a RuntimeError.

from . import config

# shorthand for the two cppyy-wrapped namespaces. Note how, without lifting
# a finger, vigra has been wrapped as well and can be used from python
# without having to rely on vigra's own python module. But vspline makes
# no attempt at exploiting the fact, and there is only a small amount of
# pythonization of the vigra cppyy wrap where it's needed to feed vspline
# with data.

vigra=cppyy.gbl.vigra
vspline=cppyy.gbl.vspline

# next we define a function which can make a multi_array_view either by
# creating a numpy array and using that to hold the data, so that the
# multi_array_view refers to those data - or by referring to data in an
# object which is passed in as the 'container' argument.
# What's nice here is that this routine will work for any base type,
# any number of channels and any number of dimensions - it's generic!
# this is due to the fact that the appropriate C++ types are specialized
# automatically via the cppyy wrap of class multi_array_view
# Again, we use a naive approach and trust the user to know what they
# are doing; passing unsuitable arguments will result in undefined
# bahaviour.
# this approach may seem a detour at first, but it enables us to hold
# the actual data in a NumPy array, rather than having to create a
# vigra::MultiArray. This keeps all bulk data allocation on the python
# side: all arrays 'are' numpy ndarrays, the parallel access via
# vspline.multi_array_view only refers to the numpy data.

# we define class 'array' which holds both the multi_array_view
# and the container the view refers to. We fit this class with
# __getitem__ and __setitem__ to make it's content conveniently accessible.
# for printing, we use str() on the numpy array. To pass the data to
# code expecting a vigra::MultiArrayView, we pass the 'as_view' member,
# for code expecting a NumPy ndarray, we pass the 'as_np' member.
# Note that both these members have a notion of the 'shape' of the data
# which is reversed: numpy goes (z,y,x) like C indices, vigra goes (x,y,z).
# The getitem and setitem methods follow vigra convention.

# TODO: I'd like to import NumPy arrays directly, but there is an issue here
# that I don't know how to deal with: let's say I want to use a dtype like
# dt = np.dtype((np.float32, (3)))
# This would represent for example a float pixel. If I create an ndarray
# with this dtype, the resulting array will have an additional dimension
# but dtype float32. While this does the memory justice and produces correct
# items when indexing, the dtype I used for creating the array is lost. So
# when trying to import an ndarray, I can't tell if a shape of three for
# one of it's axes is due to the dtype passed at creation or due to this
# axis being merely three units long.
# So I work around this by passing in the number of channels. And I silently
# assume that the element channel is the last dimension in the numpy array,
# which should be the case for "C" data. TODO check more

# I use a dictionary to find the C++ data type for the NumPy dtype,
# but this is not easy to get right; so far I only get this to work
# smoothly for trivial types:

dtype_to_cpp = { np.dtype(np.uint8) : "unsigned char" ,
                 np.dtype(np.int8) : "signed char" ,
                 np.dtype(np.uint16) : "unsigned short" ,
                 np.dtype(np.int16) : "short" ,
                 np.dtype(np.uint32) : "unsigned int" ,
                 np.dtype(np.int32) : "int" ,
                 np.dtype(np.uint64) : "unsigned long" ,
                 np.dtype(np.int64) : "long" ,
                 np.dtype(np.float32) : "float" ,
                 np.dtype(np.float64) : "double" ,
                 np.dtype(np.longdouble) : "long double" }

# TODO: int8 vs. char
# TODO: how to specify complex numbers?
                 #np.dtype(np.complex64) : cppyy.gbl.complex["float"] ,
                 #np.dtype(np.complex128) : "std::complex<double>" }

# reverse dictionary

cpp_to_dtype = { "signed char" : np.dtype(np.int8) ,
                 "unsigned char" : np.dtype(np.uint8) ,
                 "short" : np.dtype(np.int16) ,
                 "unsigned short" : np.dtype(np.uint16) ,
                 "int" : np.dtype(np.int32) ,
                 "unsigned int" : np.dtype(np.uint32) ,
                 "long" : np.dtype(np.int64) ,
                 "unsigned long" : np.dtype(np.uint64) ,
                 "float" : np.dtype(np.float32) ,
                 "double" : np.dtype(np.float64) ,
                 "long double" : np.dtype(np.longdouble) } 

# helper functions to convert numpy shapes and strides to vigra shapes
# and strides, and their reverse. The conversion is necessary because
# vigra's and NumPy's axis orders are reversed, and NumPy arrays of
# TinyVectors are expressed in terms of the element-expanded array
# with an extra dimension if there is more than one channel.
# this is true only if the numpy default axis order is used, hence
# incoming arrays with fortran memory order are reinterpreted as
# 'standard' arrays, in np_to_vigra further down. The vspline side
# always looks at the multi_array_view representation of the data.

def make_np_shape ( v_shape , channels ) :
  dimension = len ( v_shape )
  np_shape = [ v_shape [ dimension - i - 1 ] for i in range ( dimension ) ]
  if channels > 1 :
    np_shape.append ( channels )
  return tuple ( np_shape )

def make_np_stride ( v_stride , channels , sz ) :
  dimension = len ( v_stride )
  np_stride = [ channels * v_stride [ dimension - i - 1 ]
                for i in range ( dimension ) ]
  if channels > 1 :
    np_stride.append ( 1 )
  np_stride = [ sz * e for e in np_stride ]
  return tuple ( np_stride )

def make_v_shape ( np_shape , channels ) :
  dimension = len ( np_shape )
  if channels > 1 :
    if not np_shape [ -1 ] == channels :
      raise ValueError ( "need last shape element == nr. of channels: %d" %
                         ( channels, ) )
    dimension = dimension - 1 
  v_shape = [ np_shape [ dimension - i - 1 ] for i in range ( dimension ) ]
  return tuple ( v_shape )

def make_v_stride ( np_stride , channels , sz ) :
  np_et_stride = tuple ( [ l // sz for l in np_stride ] )
  dimension = len ( np_stride )
  dr = dimension
  if channels > 1 :
    if not np_et_stride [ -1 ] == 1 :
      raise ValueError ( "multichannel values must be contiguous" )
    dimension = dimension - 1 
  v_stride = [ np_et_stride [ dimension - i - 1 ] // channels
               for i in range ( dimension ) ]
  return tuple ( v_stride )


def vigra_to_np ( as_view ) :
  """vigra_to_np takes a multi_array_view and converts it to a
     numpy ndarray referring to the same data.
  """

  v_shape = as_view.shape() ;
  v_stride = as_view.stride() ;
  channels = as_view.channels ;
  np_typestr = as_view.get_typestr() ;
  np_itemsize = as_view.itemsize
  np_shape = make_np_shape ( v_shape , channels )
  np_stride = make_np_stride ( v_stride , channels , np_itemsize )

  # the easiest way to create a numpy array from another object is
  # to have an object with defined array interface, so we augment
  # as_view with the relevant information:

  as_view.__array_interface__ = { "shape" : np_shape ,
                                  "typestr" : np_typestr ,
                                  "data" : ( as_view.baseaddr , False ) ,
                                  "strides" : np_stride ,
                                  "version" : 3 }

  as_np = np.array ( as_view , copy = False )

  # now we can discard the additional information again.

  del as_view.__array_interface__

  return as_np

def np_to_vigra ( as_np , channels ) :
  """np_to_vigra takes a NumPy ndarray and creates a multi_array_view
     referring to the same data. We need the number of channels, because
     we can't work this out from the ndarray.
  """
  
  np_shape = as_np.shape
  np_stride = as_np.strides
  dtype = np.dtype(as_np.dtype)
  np_dim = len ( np_shape )

  # incoming arrays in fortran memory order (like, from julia) are expected
  # to have the channel dimension first. We reverse both shape and strides
  # and produce a new view to the same data, which is now c-contiguous
  # and suitable input for the remainder of the code.

  if as_np.flags [ 'F_CONTIGUOUS' ] == True :

    np_shape = tuple ( reversed ( np_shape ) )
    np_stride = tuple ( reversed ( np_stride ) )
    
    as_np = np.ndarray ( np_shape , as_np.dtype , as_np.ravel('K') ,
                         0 , np_stride )

  if not dtype.isbuiltin :
    raise ValueError ( "dtype must be a built-in numpy type" )

  try :
    vetype = dtype_to_cpp [ dtype ]
  except :
    raise ValueError ( "NumPy dtype %s not covered" % dtype )
  
  sz = dtype.itemsize
  
  _v_shape = make_v_shape ( np_shape , channels )
  dimension = len ( _v_shape )
  _v_stride = make_v_stride ( np_stride , channels , sz )
  
  # build vigra.TinyVectors needed to create a MultiArrayView
  
  v_shape = vigra.TinyVector [ "long" , dimension ]()
  v_stride = vigra.TinyVector [ "long" , dimension ]()
  
  # note how the stride is still in units of base_type, we need units of
  # value_type, hence yet another division, now by 'channels'
  # note how we reverse the order of shape and stride to vigra x,y,z...

  for i in range ( dimension ) :

    v_shape[i] = _v_shape [i]
    v_stride[i] = _v_stride[i]

  # to ward off mistakes in interpreting the numpy data, we now reverse
  # the process and try to rebuild the initial shape and strides

  reshape = make_np_shape ( v_shape , channels )
  restride = make_np_stride ( v_stride , channels , sz )
  
  for d in range ( np_dim ) :
    
    if np_shape[d] != reshape[d] :
      raise IndexError ( "can't reproduce original shape for dim. %d" % d )
   
    if np_stride[d] != restride[d] :
      raise IndexError ( "can't reproduce original stride for dim. %d" % d )
  
  # create a vspline.multi_array_view
  
  view_type = vspline.multi_array_view [ dimension , vetype , channels ]
  as_view = view_type ( v_shape , v_stride , as_np.ravel('K') )

  # return class array, which is the most convenient packaging, holding a
  # reference to the NumPy array as well.

  return as_view

class array :
  """convenience class holding both a vspline.multi_array_view and a
     NumPy array. These two objects are meant to refer to the same data,
     even if this is not enforced. The python functions in this module
     will try and produce this data type from arguments referring to
     chunks of data and pass the as_view member to the cppyy wrap of
     the code in vspline.h.
     See the function as_array, further down.
  """

  def __init__ ( self , as_view , as_np ) :
    
    """create a new 'array' object. Note that this isn't expected to be
       done by user code, please use the factory function 'make_array'
    """

    self.as_view = as_view
    self.as_np = as_np
    self.view_type = type ( self.as_view )
    
    ## TODO: I'd like to be able to infer the base type from the as_view,
    ## but currently I can't get this to work. I'd like plain 'float' etc.
    
    ## this gives 'class float' which is python-side
    #self.base_type = self.view_type.fundamental_type.__bases__[0]
    #print ( self.base_type )
    
    ## this gives a typestring, which is ctypes
    #self.base_type = cppyy.typeid ( self.view_type.fundamental_type ).name()    
    #print ( self.base_type )
    
  def __str__ ( self ) :
    
    """delegate to NumPy ndarray's str method"""
    
    return str ( self.as_np )
  
  def __setitem__ ( self , crd , value ) :
    
    """assign a new value 'value' to the datum located at coordinate 'crd'
       if the array is 1D, pass a plain integer for crd, otherwise a tuple
       of integers.
       note the axis order: fastest moving index *first*: (x,y,z)
       if the value is single-channel, pass a plain value, otherwise a
       tuple of values.
    """
    
    # create the object needed to index the vigra array
    
    #_crd = self.view_type.shape_type()

    # if the array's dimension is greater than 1, we're content with any
    # indexable 'crd' and fetch the components one by one.
    
    #if self.as_view.dimension > 1 :
      #_crd = self.view_type.shape_type()
      #for i in range ( self.as_view.dimension ) :
        #_crd [ i ] = crd [ i ]
    #else :
      #_crd = crd

    # we handle multichannel data in the same way:
    
    _value = self.view_type.value_type()

    if self.as_view.channels > 1 :
      for i in range ( self.as_view.channels ) :
        _value [ i ] = value [ i ]
    else :
      _value = value

    # finally we do the assignment.

    self.as_view [ crd ] = _value
  
  def __getitem__ ( self , crd ) :
    
    """obtain the datum located at coordinate 'crd'. if the array is 1D,
       pass a plain integer for crd, otherwise pass a tuple.
       note the axis order: fastest moving index *first*: (x,y,z)
       if the data are single-channel, you receive a single value,
       otherwise a tuple of values.
       If the spline's dimension is greater than one and you pass
       a single coordinate, it will refer to the flattened array.
    """

    result = self.as_view [ crd ]
    
    if self.as_view.channels > 1 :
      return tuple ( [ result[i] for i in range ( self.as_view.channels ) ] )
    else :
      return result

# TODO: reduce this function to creation of new arrays?
# can create array from np.ndarray now

def make_array ( shape ,
                 base_type = "float" ,
                 channels = 1 ) :

  """create a new array
     Note that shape is given following vigra convention (x,y,z...).
     While NumPy gives strides in bytes, vspline.array gives strides
     in units of the 'value type'. The value type is an aggregate of
     channels X base_type. if you pass base_type = "float" and channels = 3,
     the 'value type' is a packed array of three floats. So for a newly
     created vspline.array, the stride along the first axis will be 1. 
  """
  
  try :

    # if 'shape' has a length, we assume it has as many components
    # as there are axes
    
    dimension = len ( shape )
    
  except:
    
    # failing that, we assume the data are 1D

    dimension = 1

  # to create the vigra object, we need shape and stride as
  # vigra::TinyVectors of long with as many members as the number
  # of dimensions given

  v_shape_type = vigra.TinyVector [ "long" , dimension ]
    
  v_shape = v_shape_type()
  
  # we fill in the vigra shape object from the argument 'shape',
  # expecting an iterable if dimension > 1, and a plain number
  # otherwise

  if dimension > 1 :
    np_size = 1
    for i in range ( dimension ) :
      v_shape [ i ] = shape [ i ]
      np_size *= shape[i]
  else :
    v_shape[0] = shape
    np_size = shape

  np_size *= channels
  
  # now we obtain the vigra object's data type, instantiating the
  # template with the concrete template arguments gleaned from the
  # arguments
  
  view_type = vspline.multi_array_view [ dimension ,
                                         base_type ,
                                         channels ]
  
  np_dtype = cpp_to_dtype [ base_type ]
  as_np = np.zeros ( np_size , np_dtype )

  as_view = view_type ( v_shape , as_np )
  np_shape = make_np_shape ( v_shape , channels )
  as_np = as_np.reshape ( np_shape )
  
  result = array ( as_view , as_np )
  return result
  
# import the bc_codes into the module namespace:

# TODO: odd: 'from vspline import MIRROR, ...' fails

from cppyy.gbl.vspline import MIRROR, PERIODIC, REFLECT, NATURAL

def as_array ( data , channels = None ) :
  """this function takes an argument 'data' and tries to create
     a vspline array object from it. This is used to make the
     code in this module more pythonic: where arrays of data are
     expected for input or output, users can pass a variety of
     types, as long as this function has a way of creating
     a vspline.array object from them, which in turn has an
     'as_view member', which can be passed to the cppyy-wrapped
     C++ code.
     If the data are NumPy ndarrays or arrays with __array_interface__
     defined and channels is passed 'None', we're guessing the number
     of channels: we assume that a small number up to four for
     the last axis' length is in fact the number of channels, which
     holds true for stereo, RGB and RGBA data, whereas some larger
     value indicates that the number of channels is 1, and the last
     axis refers to a spatial extent.
     ndarrays with fortran memory order are now treated correctly;
     the resulting array will hold a view to the data in a 'standard'
     ndarray and the multi_array_view is set up accordingly. This makes
     it possible to feed julia arrays.
  """

  if type ( data ) is array :
    return data
  elif type ( data ) is np.ndarray :
    if channels is None :
      dimensions = length ( data.shape )
      channels = data.shape [ dimension - 1 ]
      if channels > 4 :
        channels = 1
      print ( "WARNING: guessing that number of channels is %d" % channels )
    as_view = np_to_vigra ( data , channels )
    return array ( as_view , data )
  elif issubclass ( type ( data ) , vspline.is_multi_array_view ) :
    as_np = vigra_to_np ( data )
    return array ( data , as_np )
  elif getattr ( data , "__array_interface__" , None ) is not None :
    as_np = np.array ( data , copy = False )
    if channels is None :
      dimensions = length ( data.shape )
      channels = data.shape [ dimension - 1 ]
      if channels > 4 :
        channels = 1
      print ( "WARNING: guessing that number of channels is %d" % channels )
    as_view = np_to_vigra ( as_np , channels )
    return array ( as_view , as_np )
  else :
    raise ValueError ( "can't convert '%s' to vspline.array" %
                        type ( data ) )

class bspline :
  """class holding a vspline::bspline object and a standard evaluator
     for this spline, to make it a callable evaluating the spline at a
     given position
     this class exposes the complete interface of vspline::bspline.
     Note how cppyy's run-time type specialization allows us to accept
     parameters like 'channels', 'dimension' and 'base_type', which
     in C++ are template arguments, at run-time. If the specializations
     have been used before, they are cached and execute very quickly,
     if not, having them up and running and JITed up to speed may
     take noticeable time, so to test timings, start with the second
     run.
     The spline has two members of vspline.array type, referring to
     the vspline::bspline object's 'core' and 'container' members.
     vspline::bspline's 'core' is a view to those spline coefficients
     which correspond with knot point values, so it has the same
     shape as the knot point array. The 'container' is a larger view
     which also encomapasses additional coefficients outside the
     core area which are needed to evaluate the spline near it's
     borders without having to switch to special near-border code:
     since the evaluation's support is small, vspline just puts
     a few extra values around the core coefficients, and the
     evaluation can proceed without checking for border proximity.
     once the spline has been created, you either deposit knot point
     data in it's core and call prefilter without a 'data' argument,
     or, if the knot point data reside in a compatible array, you can
     pass them to 'prefilter', which will use them instead of the
     data in the core. see 'prefilter'. This can also be used to
     'suck in' data of a different fundamental type on-the-fly.
     After prefiltering, the spline is ready for use; the simplest
     case is evaluating the spline at point locations, which can be
     done using call syntax - or single coefficient access, with [],
     please see __call__, __getitem__ and __setitem__ below.
     To obtain more specialized evaluators, class bspline provides
     the methods make_evaluator and make_safe_evaluator, which map
     to the free vspline function templates with the same name.
  """

  def __init__ ( self ,
                 base_type ,
                 channels ,
                 dimension ,
                 shape ,
                 degree = 3 ,
                 boundary = None ,
                 tolerance = -1.0 ,
                 headroom = 0 ) :

    if channels > 1 :
      self.value_type = vigra.TinyVector [ base_type , channels ]
    else :
      self.value_type = base_type

    if base_type == "double" or base_type == "long double" :
      self.rc_type = base_type
    else :
      self.rc_type = "float"

    self.shape_type = vigra.TinyVector [ "long" , dimension ]
    
    if dimension > 1 :
      self.crd_type = vigra.TinyVector [ self.rc_type , dimension ]
    else :
      self.crd_type = self.rc_type

    self.spline_type = vspline.bspline [ self.value_type , dimension ]

    self.dimension = dimension
    self.channels = channels
    self.shape = self.shape_type()
    
    # TODO: when shifting, adapt this value
    
    self.degree = degree
    self.tolerance = tolerance
    self.headroom = headroom
    
    self.bcv = self.spline_type.bcv_type()
    
    if dimension > 1 :
      if boundary is None :
        boundary = [ vspline.MIRROR for k in range ( dimension ) ]
      for i in range ( dimension ) :
        self.shape [ i ] = shape [ i ]
        self.bcv [ i ] = boundary [ i ]
    else :
      if boundary is None :
        boundary = vspline.MIRROR
      self.shape[0] = shape
      self.bcv[0] = boundary
       
    if True :
      
      container_size = self.spline_type.get_container_shape ( self.degree ,
                                                              self.bcv ,
                                                              self.shape ,
                                                              self.headroom )

      
      if dimension == 1 :
        container_size = container_size[0]
      
      self.container = make_array ( container_size ,
                                    base_type ,
                                    channels )

      self.spline = self.spline_type ( self.shape ,
                                       self.degree ,
                                       self.bcv ,
                                       self.tolerance ,
                                       self.headroom ,
                                       self.container.as_view )
    
    else : # currently unused (unreachable)
      
      # alternative way of creating the spline: this way, the memory
      # would be allocated by vspline::bspline's c'tor. We don't currently
      # use this way of allocating memory for the spline, and instead
      # create all array memory as NumPy ndarrays.

      self.spline = self.spline_type ( self.shape ,
                                       self.degree ,
                                       self.bcv ,
                                       self.tolerance ,
                                       self.headroom )
    
    self.view_type = vspline.multi_array_view [ dimension ,
                                                base_type ,
                                                channels ]
    
    v_core = self.view_type ( self.spline.core )
    np_core = vigra_to_np ( v_core )

    self.core = array ( v_core , np_core )
    
    self.ev = vspline.make_safe_evaluator ( self.spline )
    
  # TODO think about this some more. It's enticing to equip the spline
  # with a 'standard' evaluator, but this takes quite some doing. In
  # c++ vspline, I have opted against this to keep the vspline::bspline
  # object lean.

  def __call__ ( self , crd ) :
    """evaluates the spline at a *real* coordinate 'crd'
       crd can be any indexable object with a size equal to or larger
       than the spline's dimension, the numbers are interpreted as
       floating point data. This is what the spline is actually made
       for: providing an interpolated value 'between' whole coordinates.
       given a 2D spline spl2, use like this:
         spl2 ( ( 23.5 , 42.0 ) )
    """

    if self.dimension > 1 :
      _crd = self.crd_type()
      for i in range ( self.dimension ) :
        _crd [ i ] = crd [ i ]
    else :
      _crd = crd
      
    result = self.ev ( _crd )
    
    if self.channels > 1 :
      return tuple ( result[i] for i in range ( self.channels ) )
    else :
      return result
  
  def __setitem__ ( self , crd , value ) :
    """assigns 'value' to the spline coefficient at coordinate crd
       crd can be any indexable object with a size equal to or larger
       than the spline's dimension, and value can be any indexable
       object with a size greater or equal to the spline's number of
       channels. note that here 'crd' is a discrete coordinate referring
       to a specific coefficient, so crd contains integers. You can
       use this assignment to place knot point data in the spline's core
       before prefiltering or to modify the spline's coefficients after
       prefiltering.
       given a 2D spline spl2 with float pixels, use like this:
         spl2 [ ( 3 , 4 ) ] = ( 5.0 , 6.0 , 7.0 )
    """


    #if self.dimension > 1 :
      #_crd = self.shape_type()
      #for i in range ( self.dimension ) :
        #_crd [ i ] = crd [ i ]
    #else :
      #_crd = crd
      
    
    if self.channels > 1 :
      _value = self.value_type()
      for i in range ( self.channels ) :
        _value [ i ] = value [ i ]
    else :
      _value = value

    self.core [ crd ] = _value
  
  def __getitem__ ( self , crd ) :
    """obtains the value of the spline coefficient at coordinate crd
       crd can be any indexable object with a size equal to or larger
       than the spline's dimension. note that here 'crd' is a discrete
       coordinate referring to a specific coefficient, so crd contains
       integers.
       given a 2D spline spl2, use like this:
         value = spl2 [ ( 3 , 4 ) ]
    """

    
    #if self.dimension > 1 :
      #_crd = self.shape_type()
      #for i in range ( self.dimension ) :
        #_crd [ i ] = crd [ i ]
    #else :
      #_crd = crd
      
    result = self.core [ crd ]
    
    if self.channels > 1 :
      return tuple ( result[i] for i in range ( self.channels ) )
    else :
      return result
  
  def prefilter ( self , data = None , boost = 1.0 ) :
    """converts knot point data (aka original data) into b-spline
       coefficients. if 'data' is passed , the knot point data are
       expected to be held by 'data' and they are expected to have
       data with the same number of channels as the spline's data.
       different fundamental types are okay, they will be converted
       on-the-fly as the data are 'pulled in'.
       the array off 'data' must have the same shape as the spline's
       core (what was passed as it's shape to the constructor),
       typically you'd pass a multi_array_view object.
       If no 'data' are passed, the knot point data are expected to
       reside in the spline's core already.
       If a value of 'boost' other than 1.0, the default, is passed,
       the spline coefficients will be amplified with this factor.
    """
    if data is None :
      self.spline.prefilter ( boost )
    else :
      data = as_array ( data , self.channels )
      self.spline.prefilter ( data.as_view , boost )

# TODO: this might be a method of the spline as well:

def make_evaluator ( spline ,
                     dspec = None ,
                     shift = 0 ,
                     rc_type = None ,
                     safe_mode = True ) :
  """create a vspline::evaluator object from the spline 'spline' with the
      properties given by the arguments. This only exploits a subset
      of the vspline function templates which are used for the purpose,
      but should cover 99% use.
      Note that the resulting evaluator object is a 'raw' vspline
      object taking and returning vigra::TinyVectors. You may want to use
      class evaluator further below which provides pythonic access.
  """
  
  if rc_type is None :
    rc_type = spline.rc_type

  vs_dspec = vigra.TinyVector [ int , spline.dimension ]()
  if not dspec is None :
    for i in range ( spline.dimension ) :
      vs_dspec[i] = dspec[i]

  if safe_mode is False :
    return vspline.make_evaluator [ type(spline.spline) , rc_type ] (
                      spline.spline , vs_dspec , shift )
  else :
    return vspline.make_safe_evaluator [ type(spline.spline) , rc_type ] (
                      spline.spline , vs_dspec , shift )

class functor :
  """wrapper class to provide a callable from a vspline::unary_functor.
     the 'inner' object takes and returns vspline::TinyVectors, whereas
     the wrapped 'functor' object will accept any indexable and return
     a tuple.
  """
  
  def __init__ ( self , raw ) :

    # KFJ 2020-12-15 vspline::unary_functor does not define operator(),
    # even though many derived classes do by using the mixin 'callable'.
    # One such derived class is vspline::grok_type, so by 'grokking'
    # the incoming 'raw' functor, we receive a callable grok_type
    # object.

    self.raw = vspline.grok ( raw )
    self.raw_t = type ( raw )
    self.dim_in = self.raw_t.dim_in
    self.dim_out = self.raw_t.dim_out

  def __call__ ( self , crd ) :
    """evaluates a functor, accepting the incoming argument by ways
       of any indexable object with a size equal to or larger
       than the functor's dim_in. the result is returned as a tuple.
    """

    if self.raw_t.dim_in == 1 :
      _crd = self.raw_t.in_type ( crd )
    else :
      _crd = self.raw_t.in_type()
      for i in range ( self.raw_t.dim_in ) :
        _crd [ i ] = crd [ i ]
        
    result = self.raw ( _crd )

    if self.raw_t.dim_out == 1 :
      return result
    else :
      return tuple ( result[i] for i in range ( self.raw_t.dim_out ) )

class evaluator ( functor ) :
  """class evaluator wraps a vspline::evaluator in a vsp.functor to
     provide a more pythonic callable object. The resulting object
     is more suitable for python code like list comprehensions, because
     it does not take/return vigra data types
  """

  def __init__ ( self ,
                 spline ,
                 dspec = None ,
                 shift = 0 ,
                 rc_type = None ,
                 safe_mode = True ) :

    if rc_type is None :
      rc_type = spline.rc_type

    self.raw = make_evaluator ( spline , dspec , shift , rc_type , safe_mode )
    self.raw_t = type ( self.raw )

class basis_functor :
  """basis_functor wraps a vspline::basis_functor, providing pythonic
     access to the b-spline basis functions for spline degrees up to 45.
     since calculation of the basis function can be split up into a part
     which depends solely on the spline's degree and another part which
     depends on the actual argument, the access to the basis functions
     also uses a two-part approach. When the functor is created, all
     calculations depending on the spline's degree are performed;
     subsequent calls to the functor with specific arguments only need
     to perform the remaining calculations and are therefore significantly
     faster than performing the calculation of the basis function
     "from scratch".
  """
  
  def __init__ ( self , _degree = 3 , _derivative = 0 , dtype = "double" ) :
    
    self.degree = _degree
    self.derivative = _derivative
    self.functor = vspline.basis_functor [ dtype ] ( _degree , _derivative )
    
  def __call__ ( self , x ) :
    """simply calling a basis_functor with a float argument returns the
      b-spline basis function at the given location
    """
  
    return self.functor ( x )

  ## TODO: other dtypes
  
  def weights ( self , delta ) :
    """weights calculates a set of weights which can be used to evaluate
      a b-spline. Delta is the fractional part of the locus of evaluation -
      either in [0,1] for odd splines or in [-0.5,0.5] for even splines, so
      to evaluate at 3.6, for an odd spline delta is 0.6, for an even spline
      delta is -0.4. The set of weights is multiplied with the set of
      coefficients contributing to the evaluation at the given locus
      and the sum is the result of the evaluation.
      The set of weights is returned as a NumPy array.
    """

    buffer = np.empty ( ( self.degree + 1 , ) , np.double )
    self.functor.weights [ "double" , "double" ] ( buffer , delta )
    return buffer

# pull in the transform family

def index_transform ( func , target ) :
  
  """ index_transform will fill 'target' by invoking 'func' with every
      discrete nD index into 'target'. So, given a target array t:
      t [ 0 , 0 ] = func ( ( 0 , 0 ) )
      t [ 1 , 0 ] = func ( ( 1 , 0 ) )
      ...
  """
  
  if type ( func ) is functor :
    rf = func.raw
  elif type ( func ) is bspline :
    rf = func.ev
  else :
    rf = func

  rt = as_array ( target , rf.dim_out )

  vspline.transform ( rf , rt.as_view )
  
def transform ( func , source , target ) :
  
  """ transform will fill 'target' by storing the result of calling 'func'
      with the argument taken from the corresponding location in 'source'.
      So, given a source array s and a target array t, we get
      t [ 0 , 0 ] = func ( s [ 0 , 0 ] )
      t [ 1 , 0 ] = func ( s [ 1 , 0 ] )
      ...
      'source' and 'target' have to have the same shape, and 'func'
      has to accept arguments of the data type held in source, and produce
      results in the data type of 'target'.
  """
  
  if type ( func ) is functor :
    rf = func.raw
  elif type ( func ) is bspline :
    rf = func.ev
  else :
    rf = func

  rs = as_array ( source , rf.dim_in )
  rt = as_array ( target , rf.dim_out )
    
  vspline.transform ( rf , rs.as_view , rt.as_view )
  
def apply ( func , data ) :
  
  """ apply works like calling 'transform' with source == target.
      while vspline offers 'apply', we might delegate to the python
      'transform' method above, which has the same effect.
  """
  
  if type ( func ) is functor :
    rf = func.raw
  elif type ( func ) is bspline :
    rf = func.ev
  else :
    rf = func

  rt = as_array ( data , rf.dim_out )

  vspline.apply ( rf , rt.as_view )
  
  # transform ( func , data , data )
  
def remap ( source ,
            coordinates ,
            target ,
            base_type ,
            degree = 3 ,
            boundary = None ) :
  
  """ remap will create a b-spline over 'source' and fill 'target'
      by evaluating the spline at every coordinate given in 'coordinates'.
      'coordinates' and 'target' need to have the same shape, and the data
      types in 'source' and 'target' need to have the same number of
      channels - usually they will even be the same.
  """
  
  rsa = as_array ( source ) # TODO channel count is only guessed here
  rs = rsa.as_view

  # TODO: I'd like to infer the base type from rs, which is a multi_array_view,
  # having a dependent type 'fundamental_type'. but the inquiry below produces
  # a result which I can't use to infer a NumPy type from. So currently I require
  # passing in base_type
  # TODO: I'd also like to infer the number of channels if I have an ndarray incoming,
  # same problem here with the ndarray not recording the original dtype. So for now
  # no incoming ndarrays, only class vspline.array or a raw MultiArrayView 
  
  # base_type = type(rs).fundamental_type
  dimension = rs.dimension
  channels = rs.channels
  
  shape = rs.shape()
  if dimension == 1 :
    shape = shape[0]
    
  # TODO: currently only float32 coordinates
  
  bspl = bspline ( base_type , channels , dimension ,
                   shape , degree , boundary )
  
  bspl.prefilter ( rsa )
  
  transform ( bspl , coordinates , target )
  
def restore ( bspl , target ) :
  """restore restores the original knot point data from a b-spline's
     coefficients. The result is written to 'target', which has to have
     the same shape as the spline's 'core'.
  """
  rt = as_array ( target , bspl.channels )
    
  vspline.restore ( bspl.spline , rt.as_view )
  
def convolve ( input ,
               output ,
               kernel ,
               dimension ,
               channels = 1 ,
               headroom = None ,
               axis = -1 ,
               boundary = None ) :
  """convolve 'input' with the kernel passed in 'kernel', storing
     the result in 'output'. Note that contrary to convention, vspline
     expects the kernel in 'forward' order. The convention is to use
     the kernel's values back-to-front. If the kernel is symmetric,
     there is no difference, but with asymmetric kernels you may
     need to pass the kernel values in reversed order.
  """
  ri = as_array ( input , channels )
  ro = as_array ( output , channels )
    
  bcv = vigra.TinyVector [ "vspline::bc_code" , dimension ] ()
  
  if boundary is None :
    boundary = [ vspline.MIRROR for k in range ( dimension ) ]

  for i in range ( dimension ) :
    bcv[i] = boundary [ i ]

  ksz = len ( kernel )
  #karr = vigra.MultiArray [ 1 , "vspline::xlf_type" ] ( ksz )
  #karrv = vigra.MultiArrayView [ 1 , "vspline::xlf_type" ] ( karr )
  
  karr = cppyy.gbl.std.vector [ "vspline::xlf_type" ] ( ksz )
  for i in range ( ksz ) :
    karr[i] = kernel [ i ]
  
  if headroom is None:
    headroom = ksz // 2

  vspline.convolution_filter ( ri.as_view ,
                               ro.as_view ,
                               bcv ,
                               karr ,
                               headroom ,
                               axis )
  
def forward_backward_recursive_filter ( input ,
                                        output ,
                                        poles ,
                                        dimension ,
                                        channels = 1 ,
                                        tolerance = -1 ,
                                        boost = 1 ,
                                        axis = -1 ,
                                        boundary = None ) :
  
  """forward-backward recursive filter with one or several poles, storing
     the result in 'output'.
  """

  ri = as_array ( input , channels )
  ro = as_array ( output , channels )
        
  bcv = vigra.TinyVector [ "vspline::bc_code" , dimension ] ()
  
  if boundary is None :
    boundary = [ vspline.MIRROR for k in range ( dimension ) ]

  for i in range ( dimension ) :
    bcv[i] = boundary [ i ]

  npoles = len ( poles )
  #parr = vigra.MultiArray [ 1 , "vspline::xlf_type" ] ( npoles )
  #karrv = vigra.MultiArrayView [ 1 , "vspline::xlf_type" ] ( parr )
  
  parr = cppyy.gbl.std.vector [ "vspline::xlf_type" ] ( npoles )
  for i in range ( npoles ) :
    parr[i] = poles [ i ]
  
  vspline.forward_backward_recursive_filter ( ri.as_view ,
                                              ro.as_view ,
                                              bcv ,
                                              parr ,
                                              tolerance ,
                                              boost ,
                                              axis )
  
class grid_spec :
  """grid_spec objects hold a set of 1D coordinate arrays, one for each
     axis. These arrays are used to form a grid (like a mesh grid)
     currently only takes single vspline.arrays or tuples thereof
  """
  
  def __init__ ( self , axes , dtype ) :
    
    try :
      self.dimension = len ( axes )
    except :
      self.dimension = 1
      self.axes = tuple ( axes )

    self.dtype = dtype
    self.grid = vigra.TinyVector [
                 vigra.MultiArrayView [ 1 , dtype ] ,
                 self.dimension ]()

    for d in range ( self.dimension ) :
      self.grid[d] = axes[d] # .as_view

def gen_grid_eval ( grid , func , target ) :
  """gen_grid_eval stands for 'generalized grid evaluation'. this name
     is used to set it apart from grid_eval (below) which is specific for
     b-splines. gen_grid_eval evaluates 'func' at every coordinate in
     a mesh grid formed from the per-axis coordinates in 'grid'.
  """
  
  if type ( grid ) is grid_spec :
    rg = grid.grid
  else :
    rg = grid

  if type ( func ) is functor :
    rf = func.raw
  elif type ( func ) is bspline :
    rf = func.ev
  else :
    rf = func

  rt = as_array ( target , rf.dim_out )

  # vspline's 'gen_grid_eval' is deprecated, now it's just another
  # overload of vspline::transform, but TODO: calling gen_grid_eval
  # is faster to warm up
  
  vspline.gen_grid_eval ( rg , rf , rt.as_view )

  # vspline.transform ( rg , rf , rt )
  
def grid_eval ( grid , ev , target ) :
  """grid_eval evaluates the b-spline 'ev' at every coordinate in
     a mesh grid formed from the per-axis coordinates in 'grid'.
     Alternatively, you can pass a raw vspline.evaluator as 'ev'
  """
  
  if type ( grid ) is grid_spec :
    rg = grid.grid
  else :
    rg = grid

  if type ( ev ) is bspline :

    if ev.dimension == 1 :
      real_coordinate_type = grid.dtype
    else :
      real_coordinate_type = vigra.TinyVector [ grid.dtype , ev.dimension ]
    value_type = ev.value_type
    raw_ev_type = vspline.evaluator [ real_coordinate_type , value_type ]
    raw_ev = raw_ev_type ( ev.spline )
    
  else :
    
    raw_ev = ev

  rt = as_array ( target , raw_ev.dim_out )

  # vspline's 'grid_eval' is deprecated, now it's just another
  # overload of vspline::transform, but TODO: calling grid_eval
  # is faster to warm up

  vspline.grid_eval ( rg , raw_ev , rt.as_view )

  # vspline.transform ( rg , raw_ev , rt )
