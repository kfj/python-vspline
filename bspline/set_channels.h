// this header determines the channel count of 'x-els' vspline_binary.so
// will support. Again, the set provided - 1-5 channels - is generous,
// and many use cases will just need one or two specific channel counts,
// like 1 and 3 when doing image processing. Comment out stuff you
// don't want and/or follow the pattern to get more.

// provide code for single-channel data

#define CHANNELS 1

#include "declare.h"

#undef CHANNELS

// provide code for 2-channel data (like complex numbers)

#define CHANNELS 2

#include "declare.h"

#undef CHANNELS

// provide code for 3-channel data (like RGB)

#define CHANNELS 3

#include "declare.h"

#undef CHANNELS

// // provide code for 4-channel data (like RGBA)
// 
// #define CHANNELS 4
// 
// #include "declare.h"
// 
// #undef CHANNELS
// 
// // provide code for 5-channel data
// 
// #define CHANNELS 5
// 
// #include "declare.h"
// 
// #undef CHANNELS
