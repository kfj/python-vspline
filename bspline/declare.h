// this header finally produces concrete definitions/declarations,
// while the header hierarchy of set_XXX.h merely serves to permute
// DIMENSION, CHANNELS and TYPE before #including this header for
// a specfic constellation.

// with DIMENSION, the number of dimensions, CHANNELS, the number of
// channels and TYPE, the fundamental data type #defined in the headers
// which #include this header, we get a set of declarations declaring
// those things in vspline which we want precompiled code for.

// We're using using 'lazy metaprogramming':

// instead of using typedefs, we simply build our declarations by using
// macros. This way we can construct quite complex types without
// creating intermediate typedeffed types which we could not get rid of
// when re-including this header for different 'DIMENSION' etc.,
// whereas we can simply #undef preprocessor identifiers.

// If definitions instead of declarations are wanted, we drop the 'extern'
// prefix by defining PREFIX empty - for declarations we use 'extern'.
// Since we're not actually providing any specialized code but merely
// triggering the compilation of specific code into the shared library,
// declarations and definitions are identical otherwise.

#ifdef EMIT_DEFINITIONS
#define PREFIX
#else
#define PREFIX extern
#endif

// vspline 'respects the singular', so 1D and single-channel data
// use fundamentals, not a TinyVector with one element.

#if CHANNELS == 1
#define value_type TYPE
#else
#define value_type vigra::TinyVector < TYPE , CHANNELS >
#endif

#define bspl_type vspline::bspline < value_type , DIMENSION >

// using float coordinates for float data and double coordinates
// for double data.

#define rc_type TYPE

#if DIMENSION == 1
#define coordinate_type rc_type
#else
#define coordinate_type vigra::TinyVector < rc_type , DIMENSION >
#endif

// vspline::bspline object

PREFIX template class vspline::bspline < value_type , DIMENSION > ;

// prefiler variants without and with original data to 'suck in'

PREFIX template void bspl_type::prefilter
                     ( xlf_type boost = xlf_type ( 1 ) ,
                       int njobs = vspline::default_njobs ) ;

PREFIX template void bspl_type::prefilter
                     ( const vigra::MultiArrayView < DIMENSION , value_type > & ,
                       xlf_type boost = xlf_type ( 1 ) ,
                       int njobs = vspline::default_njobs ) ;

// 'raw' b-spline evaluator

PREFIX template class vspline::evaluator < coordinate_type , value_type > ;

// ev_type is the type of evaluator yielded by the factory function
// make_safe_evaluator() when it's called for the spline type above,
// and bspl_ev_type is the 'raw' b-spline evaluator.

#define ev_type vspline::grok_type < coordinate_type , value_type >
#define bspl_ev_type vspline::evaluator < coordinate_type , value_type >

// seems like C++ does actually require the default args to be given
// in the declaration, like here and for prefilter() above. This sucks,
// but it suggests that a specialization might be permitted to use
// different defaults, which would make sense...
  
// providing pre-compiled code for make_safe_evaluator does not
// produce too much of a speedup, but it's measurable.
  
PREFIX template ev_type vspline::make_evaluator < bspl_type , rc_type >
                           ( const bspl_type & bspl ,
                             vigra::TinyVector < int , DIMENSION > dspec
                             = vigra::TinyVector < int , DIMENSION > ( 0 ) ,
                             int shift = 0 ) ;
  
PREFIX template ev_type vspline::make_safe_evaluator < bspl_type , rc_type >
                           ( const bspl_type & bspl ,
                             vigra::TinyVector < int , DIMENSION > dspec
                             = vigra::TinyVector < int , DIMENSION > ( 0 ) ,
                             int shift = 0 ) ;

// specialization of 'transform'. TODO: this reveals that use of grok_type
// has more to it than mere convenience: by grokking any functor, it becomes
// compatible with this specialization of 'transform'. Without the grok,
// each distinct transform would need to be specialized individually

PREFIX template
void vspline::transform < ev_type >
     ( const ev_type & ,
       vigra::MultiArrayView < DIMENSION , value_type > & ,
       int njobs = vspline::default_njobs ,
       vspline::atomic < bool > * p_cancel = 0 ) ;

PREFIX template
void vspline::restore < DIMENSION , value_type >
     ( const bspl_type & bspl ,
       vigra::MultiArrayView < DIMENSION , value_type > & ) ;

// specialization of the two variants of grid_eval, where the first one
// is specifically for b-spline evaluators, while the second one is for
// 'grokked' functors, which can perform arbitrary operations to yield
// values for coordinates.

PREFIX template
void vspline::transform < bspl_ev_type >
     ( vspline::grid_spec < DIMENSION , rc_type > & ,
       const bspl_ev_type & ,
       vigra::MultiArrayView < DIMENSION , value_type > &  ,
       int njobs = vspline::default_njobs ,
       vspline::atomic < bool > * p_cancel = 0 ) ;

PREFIX template
void vspline::transform < ev_type >
     ( vspline::grid_spec < DIMENSION , rc_type > & ,
       const ev_type & ,
       vigra::MultiArrayView < DIMENSION , value_type > &  ,
       int njobs = vspline::default_njobs ,
       vspline::atomic < bool > * p_cancel = 0 ) ;

// transform taking an array of coordinates takes an additional
// template argument TRG_DIMENSION, which specifies the number of
// dimensions the coordinate array and the target array have -
// both must obviously be the same.
// again, you may want to comment out stuff you don't want or
// add more. Just folow the pattern. Per default, you can use
// coordinate and target arrays with up to three dimensions:
       
#define TRG_DIMENSION 1
#include "transform_with_warp.h"
#undef TRG_DIMENSION
       
#define TRG_DIMENSION 2
#include "transform_with_warp.h"
#undef TRG_DIMENSION
       
#define TRG_DIMENSION 3
#include "transform_with_warp.h"
#undef TRG_DIMENSION

// finally, #undef everything so the next #include of this header
// with a different constellation of DIMENSION, CHANNELS and TYPE
// can start afresh without a redefinition error.

#undef PREFIX
#undef ev_type
#undef value_type
#undef bspl_type
#undef bspl_ev_type
#undef coordinate_type
