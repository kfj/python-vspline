# simple configuration file for the vspline module. setting up
# is controlled by three flags, and if setting up does not succeed
# a RuntimeError is raised by cppyy.
# This module may try to load one or two shared libraries, depending
# on the flags. Please use the makefile to produce them if needed.
# If you can't create or obtain the needed shared libraries, you can
# set both use_binary and use_vc to False, which may degrade performance
# but should function just the same.
# Only set 'multithread' to False if you have good reason to do so,
# e.g. if you intend for vspline code to call back into Python.
# The bspline module can now be set up by having an object with
# the relevant attributes attached to __main__.bspline_settings
# This allows for configuration from the outside, which is a 
# convenient way to try several configurations, i.e. from a shell
# script. A good candidate for bspline_settings is an ArgumentParser.
# If no suitable definitions are found in __main__.bspline_settings,
# default values apply.

import __main__

# 'use_binary' defines whether vspline.py will try to load one of the
# variants of vspline_binary.so (set it to True) or exclusively use
# cppyy to provide executable code (set it to False):

try :
  use_binary = __main__.bspline_settings.use_binary
except :
  use_binary = False

# The next two flags define which of the possible versions of
# vspline_binary.so will be picked. You mustn't use both
# use_hwy and use_hwy - if you use none of them, you'll get
# a fallback version using small loops instead of SIMD code.

try :
  use_vc = __main__.bspline_settings.use_vc
except :
  use_vc = False

try :
  use_hwy = __main__.bspline_settings.use_hwy
except :
  use_hwy = False

# this flag determines whether vspline code will use multithreading

try :
  multithread = not __main__.bspline_settings.singlethread
except :
  multithread = True

# Now process the flags and set up accordingly. First we load shared
# libraries (if needed), and then we set some definitions.

import cppyy
import os

# get the full path of this file and extract it's directory part

bspline_path, filename = os.path.split ( __file__ )

# this is where we tell cppyy to look for include files and shared libs

cppyy.add_include_path ( bspline_path )
cppyy.add_library_path ( bspline_path )

# if use of Vc is intended, make sure that get_vc.so is present.
# If loading fails, cppyy will throw a RuntimeError. Ditto for
# use of highway.

if use_vc :

  cppyy.load_library ( "get_vc.so" )

if use_hwy :

  cppyy.load_library ( "get_hwy.so" )

# if the use of a precompiled binary for common vspline functions
# is wanted, first decide which shared library should be used, then
# load the library. If loading fails, cppyy will throw a RuntimeError

if use_binary:

  if multithread :

    if use_vc :

      vspline_binary_version = "vspline_binary_vc.so"

    elif use_hwy :

      vspline_binary_version = "vspline_binary_hwy.so"

    else :

      vspline_binary_version = "vspline_binary_novc.so"

  else :

    if use_vc :

      vspline_binary_version = "vspline_binary_vc_st.so"

    elif use_hwy :

      vspline_binary_version = "vspline_binary_hwy_st.so"

    else :

      vspline_binary_version = "vspline_binary_novc_st.so"

  cppyy.load_library ( vspline_binary_version )

# define VSPLINE_SINGLETHREAD if multithreading is not wanted, and
# define USE_VC if Vc use is intended.

if multithread is False :

  # print ( "multithreading is disabled, defining VSPLINE_SINGLETHREAD" )

  cppyy.cppdef ( "#define VSPLINE_SINGLETHREAD" )

if use_vc :

  # print ( "use_vc is set, defining USE_VC" )

  cppyy.cppdef ( "#define USE_VC" )

if use_hwy :

  # print ( "use_vc is set, defining USE_VC" )

  cppyy.cppdef ( "#define USE_HWY" )

if use_binary:

  cppyy.include ( "vspline_binary.h" )

# now we're ready to include vspline.h, be it with or without
# USE_VC defined.

# print ( "including vspline/vspline.h" )

cppyy.include ( "vspline/vspline.h" )

# pull in the recently added general digital filter routines. This code
# provides separable convolution and forward-backward multipole recursive
# filtering outside a b-spline context, which is handy if you want to use
# vspline's (very fast) code to filter large arrays: vspline automatically
# multithreads and vectorizes the filter, which should be about as fast as
# it can get.

# print ( "including vspline/general_filter.h" )

cppyy.include ( "vspline/general_filter.h" )

# we use cppyy to wrap a header file defining a C++ class template which
# inherits from vigra::MultiArrayView, but is limited to holding
# vigra::TinyVectors of fundamentals, which is the type of data vspline
# can process. Since this vigra object does not own data at all, it only
# contains a few references and is lightweight, so we can simply use the
# python representation which cppyy provides.
# Since multi_array_view inherits from vigra::MultiArrayView, we can pass
# concrete multi_array_view objects to vspline routines expecting
# vigra::MultiArrayViews.

# print ( "including multi_array_view.h" )

cppyy.include ( "multi_array_view.h" )
