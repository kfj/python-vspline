// This header determines which elementary data types will be
// covered by vspline_binary.so. The default is to process single
// and double precision floating point data: 

// process single precision floats:

#define TYPE float

#include "set_dimension.h"

#undef TYPE

// process double precision floats:

#define TYPE double

#include "set_dimension.h"

#undef TYPE
