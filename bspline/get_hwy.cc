// this trivial one-liner 'pulls in' all of the hwy code living in libhwy.a.
// It's used to create a shared library, 'get_hwy.so', which vspline.py
// looks for to determine whether hwy is available or not.

// compile like this:

// clang++ -march=native -std=c++11 -O3 -fPIC -shared -o get_hwy.so get_hwy.cc -lhwy

#include <hwy/highway_export.h>
#include <hwy/highway.h>
#include <hwy/contrib/math/math-inl.h>
#include <hwy/aligned_allocator.h>
#include "vspline/hwy_atan2.h"
