// This header determines dimensionality of data vspline_binary.so
// Will process. The default is to process 1D - 4D arrays, which is
// rather generous - most use cases won't need 3D and 4D code at all.
// vspline itself is generic, so if you need 5D, 6D, etc. just follow
// the pattern. If you want less, just comment stuff out.

// provide 1D code

#define DIMENSION 1

#include "set_channels.h"
#include "use_apply.h"

#undef DIMENSION

// provide 2D code

#define DIMENSION 2

#include "set_channels.h"
#include "use_apply.h"

#undef DIMENSION

// provide 3D code

#define DIMENSION 3

#include "set_channels.h"
#include "use_apply.h"

#undef DIMENSION

// // provide 4D code
// 
// #define DIMENSION 4
// 
// #include "set_channels.h"
// #include "use_apply.h"
// 
// #undef DIMENSION
