#   This is an example program to be used with
#
#   bspline - python module wrapping vspline: generic C++ code
#             for creation and evaluation of uniform b-splines
#   
#           Copyright 2015 - 2022 by Kay F. Jahnke
#   
#   Permission is hereby granted, free of charge, to any person
#   obtaining a copy of this software and associated documentation
#   files (the "Software"), to deal in the Software without
#   restriction, including without limitation the rights to use,
#   copy, modify, merge, publish, distribute, sublicense, and/or
#   sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following
#   conditions:
#   
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the
#   Software.
#   
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#   OTHER DEALINGS IN THE SOFTWARE.


# This program demonstrates how C++ SIMD code can be 'rolled out'
# over arrays of data using vspline's transform function. Here we
# use a variant of 'transform' called 'apply' which writes results
# back to the source array, which makes for an operation which
# 'feels' like in-place, but uses a small simdized buffer internally.
# The program does a common task: it converts from cartesian to
# polar coordinates and back. Try different configurations in config.py
# to see how this influences execution times. Another interesting
# option is to change from float to double data.

# before we import the bspline module, we scan sys.argv, which may
# contain arguments relevant to the bspline module. If there are
# any command line arguments, we set up an argument parser and
# attach it to the name 'bspline_settings'. The bspline module
# will scan the __main__ namespace for this symbol and extract
# the arguments it knows. 

import sys
import argparse

# create an argument parser

parser = argparse.ArgumentParser (
    formatter_class=argparse.RawDescriptionHelpFormatter ,
    description = "testing polar/cartesion conversion with the bspline module" )

# these arguments will be used by the bspline module

parser.add_argument('--use_vc',
                    action='store_true',
                    dest = 'use_vc' ,
                    help='use Vc for SIMD code (requires a Vc installation)')

parser.add_argument('--use_hwy',
                    action='store_true',
                    dest = 'use_hwy' ,
                    help='use highway for SIMD code (requires a highway installation)')

parser.add_argument('--singlethread',
                    action='store_true',
                    dest = 'singlethread' ,
                    help='use only one thread - default is to use multithreading')

parser.add_argument('--use_binary',
                    action='store_true',
                    dest = 'use_binary' ,
                    help='use precompiled binaries - default is to JIT all code with cppyy')

parser.add_argument('--crd_use_binary',
                    action='store_true',
                    dest = 'crd_use_binary' ,
                    help='use precompiled binaries for coordinates - default is to JIT the code with cppyy')

parser.add_argument('--check_result',
                    action='store_true',
                    dest = 'check_result' ,
                    help='echo differences between input and output > 1e6')

# create the argument parser instance, attach it to 'bspline_settings'

bspline_settings = parser.parse_args ( sys.argv[1:] )

import time
import cppyy
import bspline
import numpy as np
import math
from bspline import config

use_vc = config.use_vc
use_hwy = config.use_hwy
use_binary = config.use_binary
multithread = config.multithread

if use_hwy :
  simd = "hwy"
elif use_vc :
  simd = "vc"
else :
  simd = "none"

print ( "SIMD library: %s   use binary: %s   use bin. functors: %s   multithread: %s" %
        ( simd , use_binary , bspline_settings.crd_use_binary , multithread ) )

# use coordinates as .so or via cppyy

try :
  use_binary = bspline_settings.crd_use_binary
except :
  use_binary = False

if not multithread :
  cppyy.cppdef ( "#define VSPLINE_SINGLETHREAD" )
if use_vc :
  cppyy.cppdef ( "#define USE_VC" )
elif use_hwy :
  cppyy.cppdef ( "#define USE_HWY" )

if use_binary :

  # use_binary is true, so we only include the declarations for
  # the coordinate functionality and load a shared library providing
  # the code in binary form.

  cppyy.include ( "coordinates.h" )

  # there are six variants of the shared library (see makefile
  # how to make them) - using externally compiled functors works
  # as expected, multithreading speeds processing up by the number
  # of cores used.

  if multithread:
    if use_hwy :
      cppyy.load_library ( "coordinates_hwy.so" )
    elif use_vc :
      cppyy.load_library ( "coordinates_vc.so" )
    else :
      cppyy.load_library ( "coordinates_novc.so" )
  else :
    if use_hwy :
      cppyy.load_library ( "coordinates_hwy_st.so" )
    elif use_vc :
      cppyy.load_library ( "coordinates_vc_st.so" )
    else :
      cppyy.load_library ( "coordinates_novc_st.so" )
      
else :

  # use_binary is False, so we include 'coordinates.cc' which has
  # the C++ source code to provide the coordinates functionality
  # TODO: singlethreaded variants come out too slow

  cppyy.include ( "coordinates.cc" )

# simple timed tests, performing the application of the functor and
# a function call with an array argument. The second test is to work
# out better where the problem with singlethreaded code comes from.

def timed_test ( f , a ) :
  start = time.perf_counter()
  bspline.apply ( f , a )
  end = time.perf_counter()
  return end - start ;

def timed_test_f ( f , a ) :
  start = time.perf_counter()
  f ( a )
  end = time.perf_counter()
  return end - start ;

# grind_t is a functor performing the grind operation, so it can be
# passed to bspline.transform or bspline.apply. grinda is a function
# which applies the grind operation to an array passed to it. See
# 'coordinates.h' for the C++ code.

grind_t = cppyy.gbl.grind_t['crd64_t']()

grinda = cppyy.gbl.grinda['crd64_t']

# let's test with a large-ish array of coordinates in the range of [-5,5]

width = 1000
height = 1000
np_data = 10.0 * np.random.rand ( width * height * 2 ) - 5.0
np_data_f = np.asarray ( np_data , dtype = np.float64 )
a = np_data_f.reshape ( ( width , height , 2 ) )
ar = bspline.np_to_vigra ( a , 2 )
b = a.copy() # for reference

# showtime. note that the first iteration will be subject to
# 'warmup': the code will be JITed, which may take considerable
# time. We discard the first iteration's time measurement and add
# up the next ten.

grind_took = 0.0
grind_f_took = 0.0

for times in range ( 11 ) :

  # we do first one then the other, rather than repeating each ten
  # times, to lower possible effects of load-induced slowdown.

  grind_took += timed_test ( grind_t , ar )
  grind_f_took += timed_test_f ( grinda , ar )

  # don't use first result (warmup)

  if times == 0 :
    grind_took = 0.0
    grind_f_took = 0.0

                
print ( "grind functor: %f sec   grind function: %f sec" %
         ( grind_took , grind_f_took ) )

if bspline_settings.check_result :
  
  # look at a few data, see how well the round-trip reproduced the input
  # surpise: after a few iterations, the errors remain stable.

  for x in range ( 0 , width , 99 ) :
    for y in range ( 0 , height , 99 ) :
      delta = a [ ( x , y ) ] - b [ ( x , y ) ]
      if abs ( delta[0] ) + abs ( delta[1] ) > 1e-6 :
        print ( "ouch: @ (%d,%d) x,y = %g %g delta = %g %g" %
                ( x , y , a [ ( x , y , 0 ) ] , a [ ( x , y , 1 ) ] , delta[0] , delta[1] ) )
  
