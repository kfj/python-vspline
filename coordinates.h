// header to include conversion from 2D cartesian to polar and back

#include "bspline/vspline/vspline.h"

// functor templates to convert from cartesian to polar and back,
// and to repeat this conversion to and fro 128 times, to test
// the speed of the arithmetic - applying the conversion functors
// to arrays of data is mainly memory-bound.

template < typename crd_t >
struct to_polar_t : public vspline::unary_functor < crd_t , crd_t >
{
  template < typename I , typename O >
  void eval ( const I & in , O & out ) const
  {
    out [ 0 ] = atan2 ( in[1] , in[0] ) ;
    out [ 1 ] = sqrt ( in[1] * in[1] + in[0] * in[0] ) ;
  }
} ;

template < typename crd_t >
struct to_cart_t : public vspline::unary_functor < crd_t , crd_t >
{
  template < typename I , typename O >
  void eval ( const I & in , O & out ) const
  {
    out [ 0 ] = in[1] * cos ( in[0] ) ;
    out [ 1 ] = in[1] * sin ( in[0] ) ;
  }
} ;

template < typename crd_t >
class grind_t : public vspline::unary_functor < crd_t , crd_t >
{
  public:
  template < typename I , typename O >
  void eval ( const I & in , O & out ) const
  {
    O cart = in ; // avoid read-before-write
    O polar ;
    for ( std::size_t ntimes = 0 ; ntimes < 128 ; ntimes++ )
    {
      polar [ 0 ] = atan2 ( cart[1] , cart[0] ) ;
      polar [ 1 ] = sqrt ( cart[1] * cart[1] + cart[0] * cart[0] ) ;
      cart [ 0 ] = polar[1] * cos ( polar[0] ) ;
      cart [ 1 ] = polar[1] * sin ( polar[0] ) ;
    }
    out = cart ;
  }
} ;

// function templates to convert 2D arrays of coordinates from
// cartesian to polar and back, plus the 'grind' functor above.
// The functions receive arrays and use vspline::transform
// or vspline::apply with one of the functors above.

template < typename crd_t >
void to_polar ( const vigra::MultiArrayView < 2 , crd_t > & src ,
                vigra::MultiArrayView < 2 , crd_t > & trg )
{
  vspline::transform ( to_polar_t < crd_t > () , src , trg ) ;
}

template < typename crd_t >
void to_cart ( const vigra::MultiArrayView < 2 , crd_t > & src ,
               vigra::MultiArrayView < 2 , crd_t > & trg )
{
  vspline::transform ( to_cart_t < crd_t > () , src , trg ) ;
}

template < typename crd_t >
void grind ( const vigra::MultiArrayView < 2 , crd_t > & src ,
             vigra::MultiArrayView < 2 , crd_t > & trg )
{
  vspline::transform ( grind_t < crd_t > () , src , trg ) ;
}

template < typename crd_t >
void grinda ( vigra::MultiArrayView < 2 , crd_t > & data )
{
  vspline::apply ( grind_t < crd_t > () , data ) ;
}

#ifdef EMIT_DEFINITIONS

#define PREFIX

#else

#define PREFIX extern

#endif

// instatiations of the functor and function templates for
// single and double precision coordinates

typedef typename vigra::TinyVector < float , 2 > crd32_t ;
typedef typename vigra::TinyVector < double , 2 > crd64_t ;

PREFIX template struct to_polar_t < crd32_t > ;
PREFIX template struct to_cart_t < crd32_t > ;
PREFIX template struct grind_t < crd32_t > ;

PREFIX template void to_polar < crd32_t >
  ( const vigra::MultiArrayView < 2 , crd32_t > & src ,
    vigra::MultiArrayView < 2 , crd32_t > & trg ) ;

PREFIX template void to_cart < crd32_t >
  ( const vigra::MultiArrayView < 2 , crd32_t > & src ,
    vigra::MultiArrayView < 2 , crd32_t > & trg ) ;

PREFIX template void grind < crd32_t >
  ( const vigra::MultiArrayView < 2 , crd32_t > & src ,
    vigra::MultiArrayView < 2 , crd32_t > & trg ) ;

PREFIX template void grinda < crd32_t >
  ( vigra::MultiArrayView < 2 , crd32_t > & data ) ;

PREFIX template struct to_polar_t < crd64_t > ;
PREFIX template struct to_cart_t < crd64_t > ;
PREFIX template struct grind_t < crd64_t  > ;

PREFIX template void to_polar < crd64_t >
  ( const vigra::MultiArrayView < 2 , crd64_t > & src ,
    vigra::MultiArrayView < 2 , crd64_t > & trg ) ;

PREFIX template void to_cart < crd64_t >
  ( const vigra::MultiArrayView < 2 , crd64_t > & src ,
    vigra::MultiArrayView < 2 , crd64_t > & trg ) ;

PREFIX template void grind < crd64_t >
  ( const vigra::MultiArrayView < 2 , crd64_t > & src ,
    vigra::MultiArrayView < 2 , crd64_t > & trg ) ;

PREFIX template void grinda < crd64_t >
  ( vigra::MultiArrayView < 2 , crd64_t > & data ) ;

#undef PREFIX
