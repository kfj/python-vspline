#   This is an example program to be used with
#
#   bspline - python module wrapping vspline: generic C++ code
#             for creation and evaluation of uniform b-splines
#   
#           Copyright 2015 - 2022 by Kay F. Jahnke
#   
#   Permission is hereby granted, free of charge, to any person
#   obtaining a copy of this software and associated documentation
#   files (the "Software"), to deal in the Software without
#   restriction, including without limitation the rights to use,
#   copy, modify, merge, publish, distribute, sublicense, and/or
#   sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following
#   conditions:
#   
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the
#   Software.
#   
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#   OTHER DEALINGS IN THE SOFTWARE.


# This program demonstrates how C++ SIMD code can be 'rolled out'
# over arrays of data using vspline's transform function. Here we
# use a variant of 'transform' called 'apply' which writes results
# back to the source array, which makes for an operation which
# 'feels' like in-place, but uses a small simdized buffer internally.
# The program does a common task: it converts from cartesian to
# polar coordinates and back. Try different configurations in config.py
# to see how this influences execution times. Another interesting
# option is to change from float to float data.

import time
import cppyy
import bspline
import numpy as np
import math

# hwy_simd_type can display the vectorized type's properties:

if bspline.config.use_hwy:

  cppyy.cppdef ( """
    template < typename T >
    void v_info()
    {
      vspline::simdized_type<T> v ;
      v.info() ;
    }
    """
  )

  cppyy.gbl.v_info["float"]()

# We program the functors in C++, inheriting from vspline::unary_functor.
# Since our functors are so trivial, we can use a template to cover both
# vectorized and scalar arguments, so we only have a single 'eval'
# member function template. The first functor converts cartesian to polar
# coordinates, the second does the reverse operation. Since both
# types of coordinates have two components, we can use the same
# array for input and output.

cppyy.cppdef ( """
  typedef vigra::TinyVector < float , 2 > crd_t ;
  struct to_polar_t : public vspline::unary_functor < crd_t , crd_t >
  {
    template < typename I , typename O >
    void eval ( const I & in , O & out ) const
    {
      out [ 0 ] = atan2 ( in[1] , in[0] ) ;
      out [ 1 ] = sqrt ( in[1] * in[1] + in[0] * in[0] ) ;
    }
  } ;
  """
)

cppyy.cppdef ( """
  typedef vigra::TinyVector < float , 2 > crd_t ;
  struct to_cart_t : public vspline::unary_functor < crd_t , crd_t >
  {
    template < typename I , typename O >
    void eval ( const I & in , O & out ) const
    {
      out [ 0 ] = in[1] * cos ( in[0] ) ;
      out [ 1 ] = in[1] * sin ( in[0] ) ;
    }
  } ;
  """
)

cppyy.cppdef ( """
  typedef vigra::TinyVector < float , 2 > crd_t ;
  
  void to_polar ( const vigra::MultiArrayView < 2 , crd_t > & src ,
                  vigra::MultiArrayView < 2 , crd_t > & trg )
  {
    vspline::transform ( to_polar_t() , src , trg ) ;
  }
  
  void to_cart ( const vigra::MultiArrayView < 2 , crd_t > & src ,
                 vigra::MultiArrayView < 2 , crd_t > & trg )
  {
    vspline::transform ( to_cart_t() , src , trg ) ;
  }
  """
)

# we'll use shorter names for the two functors. Note how we create
# instances of the cppyy-wrapped classes

c2p = cppyy.gbl.to_polar_t()
p2c = cppyy.gbl.to_cart_t()

# simple timed test, performing the application of the functor

def timed_test ( f , a ) :
  start = time.perf_counter()
  bspline.apply ( f , a )
  end = time.perf_counter()
  return end - start ;

def timed_test_f ( a ) :
  start = time.perf_counter()
  cppyy.gbl.to_polar ( a , a )
  end = time.perf_counter()
  return end - start ;

def timed_test_r ( a ) :
  start = time.perf_counter()
  cppyy.gbl.to_cart ( a , a )
  end = time.perf_counter()
  return end - start ;

# let's use the functor on a large-ish array of coordinates
# in the range of [-5,5]

width = 10000
height = 10000
np_data = 10.0 * np.random.rand ( width * height * 2 ) - 5.0
np_data_f = np.asarray ( np_data , dtype = np.float32 )
a = np_data_f.reshape ( ( width , height , 2 ) )
b = a.copy() # for reference

# showtime. note that the first iteration will be subject to
# 'warmup': the code will be JITed, which may take considerable
# time. We use 'apply' several times, which will eventually
# degrade the data so that we get 'ouches' when comparing them
# to the original data - an ideal process would reproduce the
# original data every time. Note also that 'setting up' takes
# considerable times - the program pulls in a lot of complex
# code and processes it before it can start executing anything.
# We expect more 'ouches' as the program proceeds, because every
# iteration feeds back the result of the previous one, so the
# errors can accumulate.

ar = bspline.np_to_vigra ( a , 2 )

for times in range ( 5 ) :

  # here we explore two different types of applying the functors
  # to the data: the first one passes functor and array to 'apply',
  # the next one has the call to 'apply' in the C++ code and only
  # passes the array (wrapped in a bspline.array)

  c2p_took = timed_test ( c2p , a )
  p2c_took = timed_test ( p2c , a )

  print ( "passing f: c2p took %f sec, p2c took %f, total = %f" %
         ( c2p_took , p2c_took , c2p_took + p2c_took ) )

  c2p_took = timed_test_f ( ar )
  p2c_took = timed_test_r ( ar )

  print ( "passing a: c2p took %f sec, p2c took %f, total = %f" %
         ( c2p_took , p2c_took , c2p_took + p2c_took ) )

# look at a few data, see how well the round-trip reproduced the input

  for x in range ( 0 , width , 99 ) :
    for y in range ( 0 , height , 99 ) :
      delta = a [ ( x , y ) ] - b [ ( x , y ) ]
      if abs ( delta[0] ) + abs ( delta[1] ) > 1e-5 :
        print ( "ouch: @ (%d,%d) delta = %g %g" %
                ( x , y , delta[0] , delta[1] ) )
