# This little program demonstrates 'imbuing' an array with output of
# a functor. For this special case, we'll use a functor whose output
# is it's input, and we'll use vspline.index_transform as the imbuing
# method: each member of the target array will be set to the output
# of the functor fed with the element's discrete coordinate. The
# combined effect is to fill the array with it's elements' coordinates.
# To do so is trivial, but to do so as fast as possible requires
# hardware vectorization and multithreading, and that's where vspline's
#' transform' family of functions comes in. Of course, for this example
# such artistry is futile, but if the array size is large, it makes
# all the difference. Try throwing out the echo to stdout and raising
# array size to half a billion, and you'll see how fast the code is. When
# 'watching' with a process monitor, you'll notice single-threaded activity
# in the beginning, which is 'setting up' python and the complex modules
# used for this example. Then you'll get two runs where all cores are
# in full use. That's when this example executes the index_transforms:
# vspline employs a thread pool to run threads with vector code in
# parallel.
# While playing with the functor, also notice how the 'inline' C++ code
# for the functor needs no explicit compilation. This makes for quick
# turn-around times: just change the inline C++ code and re-run.

import cppyy
import bspline
import numpy as np

# We program the functor in C++, inheriting from vspline::unary_functor.
# Since our functor is so trivial, we can use a template to cover both
# vectorized and scalar arguments. To show 'what's passing through' we
# echo the input to std::cout. We'll see that, initially, vectors of
# several (like 4 or 8) numbers are processed, followed by single
# numbers once there are less than a vector's fill left.

cppyy.cppdef ( """
  template < typename T >
  struct imbue_t : public vspline::unary_functor < T >
  {
    template < typename I , typename O >
    void eval ( const I & in , O & out ) const
    {
      std::cout << in << std::endl ;
      out = in ;
    }
  } ; """
)

# let's use the functor on an array of doubles

a = np.empty ( 35 , np.float64 )
f = cppyy.gbl.imbue_t["double"]()
bspline.index_transform ( f , a )
print ( a )

# use it on an array of floats - notice how the vector width
# is now longer.

a = np.empty ( 35 , np.float32 )
f = cppyy.gbl.imbue_t["float"]()
bspline.index_transform ( f , a )
print ( a )
