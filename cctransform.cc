/************************************************************************/
/*                                                                      */
/*    vspline - a set of generic tools for creation and evaluation      */
/*              of uniform b-splines                                    */
/*                                                                      */
/*            Copyright 2015 - 2018 by Kay F. Jahnke                    */
/*                                                                      */
/*    Permission is hereby granted, free of charge, to any person       */
/*    obtaining a copy of this software and associated documentation    */
/*    files (the "Software"), to deal in the Software without           */
/*    restriction, including without limitation the rights to use,      */
/*    copy, modify, merge, publish, distribute, sublicense, and/or      */
/*    sell copies of the Software, and to permit persons to whom the    */
/*    Software is furnished to do so, subject to the following          */
/*    conditions:                                                       */
/*                                                                      */
/*    The above copyright notice and this permission notice shall be    */
/*    included in all copies or substantial portions of the             */
/*    Software.                                                         */
/*                                                                      */
/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND    */
/*    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   */
/*    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          */
/*    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       */
/*    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      */
/*    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      */
/*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     */
/*    OTHER DEALINGS IN THE SOFTWARE.                                   */
/*                                                                      */
/************************************************************************/

// run prefilter and index-based transform 32 times each for
// a full HD image with 32bit RGB pixels. This is to provide a reference
// speed measurement to compare with the results from running
// 'image_processing.py' in this directory.

// to compile without Vc:
// clang++ -std=c++11 -Ofast -pthread cctransform.cc -o cctransform

// to compile with Vc:
// clang++ -DUSE_VC -std=c++11 -O3 -pthread cctransform.cc -o cctransform -lVc

// to run, simply issue ./cctransform

#include <ctime>
#include <chrono>
#include <iostream>

#include "vspline/vspline.h"

int main ( int argc , char * argv[] )
{
  typedef vigra::TinyVector < float , 3 > pixel_type ;
  typedef vigra::TinyVector < long , 2 > shape_type ;
  typedef vigra::MultiArray < 2 , pixel_type > array_type ;

  shape_type shape ( 1920 , 1080 ) ;
  array_type data ( shape ) ;

  typedef vspline::bspline < pixel_type , 2 > spline_type ;

  spline_type bspl ( shape ) ;

  // the spline's core holds all zeros, we're not interested in the
  // content, only the execution time of the call to 'prefilter' and
  // transform.

  std::cout << "******** testing prefilter" << std::endl ;
  
  for ( int cycle = 0 ; cycle < 32 ; cycle++ )
  {
    std::chrono::system_clock::time_point start
      = std::chrono::system_clock::now() ;

    bspl.prefilter() ;

    std::chrono::system_clock::time_point end
      = std::chrono::system_clock::now() ;

    std::cout << "prefilter: "
              << std::chrono::duration_cast<std::chrono::milliseconds>
                              ( end - start ) . count()
              << " ms" << std::endl ;
  }

  auto ev = vspline::make_safe_evaluator ( bspl ) ;

  std::cout << "******** testing transform" << std::endl ;
  
  for ( int cycle = 0 ; cycle < 32 ; cycle++ )
  {
    std::chrono::system_clock::time_point start
      = std::chrono::system_clock::now() ;

    vspline::transform ( ev , data ) ;

    std::chrono::system_clock::time_point end
      = std::chrono::system_clock::now() ;
      
    std::cout << "transform: "
              << std::chrono::duration_cast<std::chrono::milliseconds>
                              ( end - start ) . count()
              << " ms" << std::endl ;
  }
}
