# this example demonstrates two features:
# - interoperability with python data sporting __array_interface__
# - 'sucking in' of differently-typed data into a spline's core
#   via 'prefilter'

import sys

from PIL import Image
img = Image.open ( sys.argv[1] )
sz = img.size

import bspline
import numpy as np

# we create a bspline object holding double precision data

spl = bspline.bspline ( "double", 3 , 2 , sz )

# now we can pass 'img', which has __array_interface__ defined,
# to 'prefilter' without conversion, even though the image data
# are of some pixel type like 3 X uchar8. The __array_interface__
# information allows the vspline module to figure out the layout
# if img's data, and 'prefilter' can 'suck in' differently typed
# data, converting them on-the-fly.

spl.prefilter ( img )

# now we have a double-precision spline!

# TODO: might do something with the spline...

print ( spl.core.as_np )
